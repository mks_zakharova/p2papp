package com.example.dkolosovskiy.payapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity {
    public static final String KEY_OVERLAY = "KEY_OVERLAY";

    private Button btnEasy;
    private Button btnDev;
    private Button btnDevOverlay;


    /***/
    @Override
    protected void onCreate(Bundle savedInstanceState) {         super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_layout);
        btnEasy = findViewById(R.id.buttonTest);
        btnDev = findViewById(R.id.buttonDev);
        btnDevOverlay = findViewById(R.id.buttonDevOverlay);
        btnEasy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DevMain.class);
                startActivity(intent);
            }
        });
        btnDev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PermissionsChecker.class);
                intent.putExtra(KEY_OVERLAY, false);
                startActivity(intent);
            }
        });
        btnDevOverlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PermissionsChecker.class);
                intent.putExtra(KEY_OVERLAY, true);
                startActivity(intent);
            }
        });
        Cursor c = getContentResolver().query(Uri.parse("content://sms"), null, null, null, null);
        Logger.lg(c.getCount() + " ");
        if (c != null && c.moveToFirst()) {
            do {
                String s = "";
                for (int i = 0; i < c.getColumnCount() - 1; i++) {
                    s = s + c.getColumnName(i) + ": " + c.getString(i) + " " ;
                }
                String id = c.getString(c.getColumnIndex("_id"));
                String type = c.getString(c.getColumnIndex("type"));
                String number = c.getString(c.getColumnIndex("address")).trim();
                String status = c.getString(c.getColumnIndex("status")).trim();
                String body = c.getString(c.getColumnIndex("body")).trim();
                Logger.lg("Message " + s);
            } while (c.moveToNext());
        }
    }
}
