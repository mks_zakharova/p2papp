package com.example.dkolosovskiy.payapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.p2plib2.PayLib;
import com.p2plib2.common.CommonFunctions;
import com.p2plib2.ussd.USSDController;

public class TestActivityOverlay extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private Switch chooser;
    private EditText num;
    public int simNumber = 0;
    private EditText sum;
    private Button btn;
    static String operation = Operation.SMS.toString();
    private TextView tv;
    static Handler handler;

    final String[] operators = new String[]{"MTS", "MEGAFON", "BEELINE", "TELE"};
    String number;
    String summa;
    PayLib main;
    static Context cnt;
    String operName;
    int simNum;
    String operDest;

    enum Operation {
        SMS, USSD
    }

    String regex = "[0-9]+";
    Long operationId;

    private Intent svc;

    public void muteAudio(){
        AudioManager alarmManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_MUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_MUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_MUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_MUTE, 0);
            } else {
                alarmManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
                alarmManager.setStreamMute(AudioManager.STREAM_ALARM, true);
                alarmManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                alarmManager.setStreamMute(AudioManager.STREAM_RING, true);
                alarmManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
            }
        }
    }

    public void unMuteAudio(){
        AudioManager alarmManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.   M) {
                alarmManager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_UNMUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_UNMUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_UNMUTE, 0);
                alarmManager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_UNMUTE, 0);
            } else {
                alarmManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
                alarmManager.setStreamMute(AudioManager.STREAM_ALARM, false);
                alarmManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                alarmManager.setStreamMute(AudioManager.STREAM_RING, false);
                alarmManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
            }
        }
    }

    private void openSettingsOverlay() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Разрешение на отрисовку поверх других приложений");
        ApplicationInfo applicationInfo = getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        String name = applicationInfo.labelRes == 0 ?
                applicationInfo.nonLocalizedLabel.toString() : getString(stringId);
        alertDialogBuilder
                .setMessage("Вы должны разрешить приложению '" + name + "' отрисовываться поверх других приложений.");
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNeutralButton("Хорошо", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        if (alertDialog != null) {
            alertDialog.show();
        }
    }

    private boolean verifyOverLay() {
        boolean m_android_doesnt_grant = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(this);
        if (m_android_doesnt_grant) {
            openSettingsOverlay();
            return false;
        } else
            return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);
        final Activity act = this;
        cnt = this;
        Intent intent = getIntent();
        getOperatorParam(intent.getStringExtra("operator"));
        /**Show message */
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String text = (String) msg.obj;
                Logger.lg("text " + text);
                tv.setText(text);
            }
        };
        /**ini*/
        num = findViewById(R.id.num);
        sum = findViewById(R.id.sum);
        chooser = findViewById(R.id.switch2);
        btn = findViewById(R.id.button);
        tv = findViewById(R.id.textView2);
        if (chooser != null) {
            chooser.setOnCheckedChangeListener(this);
        }
        /**Lib ini*/
        operation = Operation.SMS.toString();
        final CallSmsResult smsResult = new CallSmsResult();
        main = new PayLib(act, cnt, smsResult, true);
        /**MTS Special*/
        final AlertDialog.Builder builderOperator = new AlertDialog.Builder(cnt);
        builderOperator.setTitle("Choose operator for destination ussd");
        builderOperator.setItems(operators, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                operDest = operators[which];
                Logger.lg("Choose " + operDest + " ");
                operationId = System.currentTimeMillis();
                svc = new Intent(TestActivityOverlay.this, SplashLoadingService.class);
                muteAudio();
                startService(svc);
                main.operation(operationId, operation.toUpperCase(), act, cnt, operDest, number, summa, simNum);
                dialog.dismiss();
                dialog.cancel();
            }
        });
        /***/
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (operation.equals(Operation.USSD.toString()) && !verifyOverLay()) { // Если USSD и нет включения рисования поверх - запрос по-новой
                    return;
                }
                if (num.getText() != null && num.getText().toString() != ""
                        && sum.getText() != null && sum.getText().toString() != "") {
                    number = num.getText().toString();
                    summa = sum.getText().toString();
                    if (number.length() == 10 && number.matches(regex)) {
                        if (operName.contains("MTS") && operation.equals(Operation.USSD.toString())) {
                            if (USSDController.isAccessiblityServicesEnable(act)) {
                                AlertDialog alertD = builderOperator.create();
                                alertD.show();
                            } else {
                                Message msg = new Message();
                                msg.obj = "Code P2P-015: Пожалуйста, разрешите приложению доступ в разделе \'Специальные возможности\' (для вызова окна перехода можно нажать кнопу <Обновить данные...>)";
                            }
                        } else {
                            Logger.lg(" number " + number + "  summa " + summa
                                    + "  " + operation + " sim num " + simNum);
                            operationId = System.currentTimeMillis();
                            svc = new Intent(TestActivityOverlay.this, SplashLoadingService.class);
                            muteAudio();
                            startService(svc);
                            main.operation(operationId, operation.toUpperCase(), act, cnt, "empty", number, summa, simNum);
                        }
                    } else {
                        Toast.makeText(cnt, "Неверный формат номера или суммы",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(cnt, "Неверный формат номера или суммы",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void getOperatorParam(String str) {
        Logger.lg("str " + str + "  " + str.trim().substring(str.indexOf("sim") + 3));
        operName = CommonFunctions.formatOperName(str.trim().substring(0, str.indexOf("sim")).trim());
        simNum = Integer.parseInt(str.trim().substring(str.indexOf("sim ") + 4).trim());
    }

    public class CallSmsResult implements com.p2plib2.CallSmsResult {
        @Override
        public void callResult(String str) {
            Logger.lg("catch " + str + " " + str.contains("-001:"));
            if (str.contains("Process ") && !str.contains("Process null")) {
                btn.setEnabled(true);
                Logger.lg(" вып  " + str);
                if (!str.toLowerCase().contains("жида") && !str.toLowerCase().contains("принят") && !str.contains("-001:")
                        && !str.toLowerCase().contains("ответ") && !str.toLowerCase().contains("к оплате")
                        && !str.toLowerCase().contains("ля подтверждения")) {
                    Intent intent = new Intent(TestActivityOverlay.this, Result.class);
                    intent.putExtra("Result", formatResult(str));
                    if (str.contains("Process ") && !str.contains("Process null")) {
                        String sy = str.toLowerCase().replaceAll("\\[", "").replaceAll("\\]", "").trim()
                                .substring(str.indexOf("process ") + 8);
//                        Logger.lg("CODE "  + " " +  sy.trim().substring(0, str.indexOf("code")));
                        try {
                            //main.clearProcess(sy);
                        } catch (Exception e) {
                            Logger.lg(" error " + sy + "  " + e.getMessage());
                        }
                    }
                    Logger.lg("fgrt");
                    main.checkSmsAdditional("");
                    if (svc != null) {
                        stopService(svc);
                        unMuteAudio();
                    }
                    startActivity(intent);
                }
            } else if(str.toLowerCase().contains("платеж за услуги связи принят")){
                Intent intent = new Intent(TestActivityOverlay.this, Result.class);
                intent.putExtra("Result", formatResult(str));
                startActivity(intent);
                if (svc != null) {
                    stopService(svc);
                    unMuteAudio();
                }
            }
            else if (str.contains("-001:")) {
                btn.setEnabled(true);
            }
        }
    }

    private String formatResult(String str) {
        Logger.lg("Before format " + str);
        String result;
        if (str.toLowerCase().contains("не дост") ||
                str.toLowerCase().contains("не про") || str.toLowerCase().contains("отказ")
                || str.toLowerCase().contains("не осущ") || str.toLowerCase().contains("не выполн")
                || str.toLowerCase().contains("не успеш") || str.toLowerCase().contains("не удал")
                || str.toLowerCase().contains("ошиб")
                || str.toLowerCase().contains("неверн")
                || str.toLowerCase().contains("не мож")
                || str.toLowerCase().contains("нельз")
                || str.toLowerCase().contains("заблокир")
                || str.toLowerCase().contains("поздн")
                || str.toLowerCase().contains("недоступ")
                || str.toLowerCase().contains("завершен")
                || str.toLowerCase().contains("истек") ) {
            if (str.toLowerCase().contains("ошибка отпра") || str.toLowerCase().contains("error:") ) {
                result = "Error: Платеж не выполнен по причине: " + str.substring(str.indexOf("] Code") + 2);
            } else {
                result = "Error: Платеж не выполнен по причине: " + str;
            }
        } else if (!str.toLowerCase().contains("жида") && !str.toLowerCase().contains("принят.") && !str.contains("-001:") && !str.toLowerCase().contains("истек")  && !str.toLowerCase().contains("error:") ) {
            result = "Сообщение о доставке:  Успешно\n" +
                    "Сумма в " + sum.getText().toString() + " переведена на номер "
                    + " " + num.getText().toString();
        } else if(str.toLowerCase().contains("платеж принят")){
            result = "Сообщение о доставке:  Успешно\n" +
                    "Сумма в " + sum.getText().toString() + " переведена на номер "
                    + " " + num.getText().toString();
        }else {
            result = str;
        }
        Logger.lg("fb " + str);
        return result;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            operation = Operation.USSD.toString();
        } else {
            operation = Operation.SMS.toString();
        }
        Message msg = new Message();
        msg.obj = operation;
        handler.sendMessage(msg);
        Toast.makeText(this, "Отслеживание переключения: " + (isChecked ? "USSD" : "SMS"),
                Toast.LENGTH_SHORT).show();
    }
}
